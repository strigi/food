import mongoose from 'mongoose'

const schema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true,
    },

    brand: {
        type: String,
    },

    propertiesPer100g: {
        energy: {
            type: Number,
            required: true,
        },

        fat: {
            type: Number,
            required: true,
        },

        carbohydrate: {
            type: Number,
            required: true,
        },

        protein: {
            type: Number,
            required: true,
        },

        fiber: Number,

        sodium: Number,

        calcium: Number,
    }
});

schema.methods.macros = function(quantity) {
    return {
        energy: this.propertiesPer100g.energy * quantity / 100,
        fat: this.propertiesPer100g.fat * quantity / 100,
        carbohydrate: this.propertiesPer100g.carbohydrate * quantity / 100,
        protein: this.propertiesPer100g.protein * quantity / 100,
        fiber: (this.propertiesPer100g.fiber ?? 0) * quantity / 100,
    }
}

export const Nutrient = mongoose.model('nutrient', schema);
