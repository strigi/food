import Router from 'koa-router';
import {Nutrient} from './nutrient.model.js'
import {Consumption} from "./consumption.model.js";
import { build } from './api-builder/index.js';
import * as db from "./database.js";

db.connect()

const router = new Router();

router.use(build(Nutrient).prefix('/nutrients').routes())
router.use(build(Consumption).prefix('/consumptions').routes())

export default router;
