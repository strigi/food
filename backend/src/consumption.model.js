import mongoose from 'mongoose';

const schema = new mongoose.Schema({
    time: {
        type: Date,
        required: true,
    },

    nutrient: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'nutrient',
        required: true,
    },

    quantity: {
        type: Number,
        required: true,
    }
});

schema.methods.macros = function() {
    return this.nutrient.macros(this.quantity);
}

export const Consumption = mongoose.model('consumption', schema);
