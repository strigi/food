import Koa from 'koa'
import api from './api.js'

const app = new Koa();

app.use(api.prefix('/api').routes())

const port = process.env.PORT;
await app.listen({ port }, () => {
    console.log(`Ready and listening on port ${port}`)
});
