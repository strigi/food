import Router from 'koa-router'

import * as utils from './http-utils.js'
import {hydratePopulation} from './hydrations/population.js'
import {hydrateSorting} from './hydrations/sorting.js'
import {hydrateFiltering} from './hydrations/filtering.js'
import {hydratePaging} from './hydrations/paging.js'
import mongoose from "mongoose";

export function build(Model) {
    const router = new Router();

    const type = 'application/json';

    router.get('/:id', utils.offer(type), async ctx => {
        const id = utils.paramMatch(ctx, 'id', /^[0-9a-f]{24}$/);
        const document = await Model.findById(id);
        ctx.status = 200;
        ctx.response.body = document;
    });

    router.get('/', utils.offer(type), async ctx => {
        const query = Model.find();
        const filtering_info = hydrateFiltering(ctx, query);
        const sorting_info = hydrateSorting(ctx, query);
        const population_info = hydratePopulation(ctx, query);
        const paging_info = await hydratePaging(ctx, query);

        let results;
        try {
            results = await query.exec();
        } catch(e) {
            captureClientErrors(ctx, e);
        }

        ctx.body = {
            results,
            $sorting_info: sorting_info,
            $filtering_info: filtering_info,
            $population_info: population_info,
            $paging_info: paging_info,
        }
    });

    router.post('/', utils.accept(type), utils.offer(type), async ctx => {
        try {
            const document = await new Model(ctx.request.body).save();
            ctx.status = 201
            ctx.response.body = document
        } catch (error) {
            if (error.code === 11000) {
                // MongoDB Duplicate key error
                ctx.throw(400, 'Duplicate error')
            }
            if (error instanceof mongoose.Error.ValidationError) {
                // Mongoose Schema validation error
                console.info('Validation error', error);
                ctx.throw(400, 'Validation error')
            }
            throw error;
        }
    });

    router.delete('/:id', async ctx => {
        const id = ctx.params.id;
        const match = /^[0-9a-f]{24}$/.exec(id)
        if (!match) {
            ctx.throw(400, 'Invalid id');
        }

        const {acknowledged, deletedCount} = await Model.deleteOne({_id: id});
        if (!acknowledged || deletedCount !== 1) {
            ctx.throw(404, `No element with id '${id}' found`);
        }
        ctx.status = 204
    });

    return router;
}

function captureClientErrors(ctx, e) {
    if(/^Cannot populate path `\w+` because it is not in your schema\..*$/.test(e.message)) {
        ctx.throw(400, 'Invalid populate')
    }

    throw e;
}

