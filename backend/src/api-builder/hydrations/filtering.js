/**
 * Parses HTTP query parameters from with name `filter` like the following examples:
 * ```
 * foo=bar (string equals)
 * foo~bar (string matches regex)
 * foo>10 (number greater than)
 * foo=3.1415 (number equals)
 * etc...
 * ```
 * @param ctx
 * @param mongoose_query
 * @returns {*[]}
 */
export function hydrateFiltering(ctx, mongoose_query) {
    const filters = extract_filters_from_request(ctx).map(parse_filter);

    // TODO: coalesce same names (either in the query param step or coalesce filters and make filter multi operator capable
    let query = mongoose_query;
    for(const filter of filters) {
        query = query.where(filter.name)[filter.method](filter.value)
    }

    console.debug('filtering_info', filters);
    return filters
}

function extract_filters_from_request(ctx) {
    const filter_param_value = ctx.query.filter
    if (!filter_param_value) {
        return []
    }

    if (!Array.isArray(filter_param_value)) {
        return [filter_param_value]
    }

    return filter_param_value
}

function parse_filter(filter_string) {
    const parse_rules = [{
        // TODO: add date (very similar to number)
        parse_rule_name: 'numbers',
        pattern: /^(\w+)(=|<|>|>=|<=)(\d+(\.\d+)?)$/,
        map: {
            '=': { method: 'eq', converter: Number },
            '>': { method: 'gt', converter: Number },
            '<': { method: 'lt', converter: Number },
            '>=': { method: 'gte', converter: Number },
            '<=': { method: 'lte', converter: Number },
        },
    }, {
        parse_rule_name: 'strings',
        pattern: /^(\w+)([=~])(\w+)$/,
        map: {
            '=': { method: 'eq', converter: String },
            '~': { method: 'eq', converter: RegExp },
        },
    }]

    for(const parse_rule of parse_rules) {
        const match = parse_rule.pattern.exec(filter_string);
        if(!match) {
            continue;
        }
        const [, name, operator_symbol, value_string] = match;

        const operator = parse_rule.map[operator_symbol]
        const value = operator.converter(value_string)
        const method = operator.method
        return { name, method, value }
    }
}

function try_match(regex, value_converter_fn) {

}