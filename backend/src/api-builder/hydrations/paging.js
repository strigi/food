import {queryInteger, queryStringList} from "../http-utils.js";

/**
 * Takes any Mongoose `Query` object and executes it with paging parameters.
 * @param mongooseQuery {Query}
 * @param ctx HTTP Request
 */
export async function hydratePaging(ctx, mongooseQuery) {
    const page = queryInteger(ctx, 'page', { min: 1, default_value: 1 })
    const page_size = queryInteger(ctx, 'page_size', { min: 1, max: 100, default_value: 10 })

    const count = await mongooseQuery.clone().count().exec();

    // If the query returns no results, numbers like `first`, `last`, `page` have no meaning.
    // Also, there needs to be no hydration done, because there is nothing to retrieve.
    if(count === 0) {
        return [
            mongooseQuery,
            undefined,
        ]
    }

    const page_count = Math.ceil(count / page_size);
    const first = ((page - 1) * page_size) + 1
    const last = Math.min(page * page_size, count)

    if(page > page_count) {
        ctx.throw(400, `Query parameter 'page' must be between '1' and '${page_count}' but was '${page}'`);
    }

    const hydrated_query = mongooseQuery.clone()
        .skip(first - 1)
        .limit(page_size)

    const paging_info = {
        count,
        first,
        last,
        page_count,
        page,
        page_size
    };

    console.debug('paging_info', paging_info)
    return paging_info
}
