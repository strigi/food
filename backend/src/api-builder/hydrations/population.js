import {queryStringList} from "../http-utils.js";

export function hydratePopulation(ctx, mongoose_query) {
    const populate = queryStringList(ctx, 'populate', { default_value: []})
    mongoose_query.populate(populate)

    console.debug('population_info', populate)
    return populate
}