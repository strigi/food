export function hydrateSorting(ctx, base_query) {
    const sort_param = ctx.query.sort ?? '+_id';

    const match = /^([+\-])?\w+$/.exec(sort_param)
    if(!match) {
        ctx.throw(400, 'Invalid sort')
    }

    const sort_direction = sort_param.startsWith('-') ? 'descending' : 'ascending'
    const sort_field_name = sort_param.replace(/[+\-]/, '');

    const sort_spec = {
        [sort_field_name]: sort_direction
    };

    base_query.sort(sort_spec)

    const sorting_info = {
        field_name: sort_field_name,
        direction: sort_direction
    };

    console.debug('sorting_info', sorting_info)
    return sorting_info
}