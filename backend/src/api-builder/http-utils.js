import bodyParser from 'koa-bodyparser';
import joi from 'joi';

export function queryStringList(ctx, query_param_name, {min, max, default_value} = {}) {
    joi.assert(ctx, joi.object().required())
    joi.assert(query_param_name, joi.string().trim().required())
    joi.assert(min, joi.number().positive().integer())
    joi.assert(max, joi.number().positive().integer())
    joi.assert(default_value, joi.array().required().items(joi.string().trim()))

    const query_param_value = ctx.query[query_param_name]
    if(!query_param_value) {
        if(!default_value) {
            ctx.throw(400, `Query param '${query_param_name}' is required`)
        }
        return default_value
    }

    if(!/^(\w+,)*\w+$/.test(query_param_value)) {
        ctx.throw(400, `Query param '${query_param_name}' is invalid`)
    }

    const values = query_param_value.split(',');
    if(min && values.length < min) {
        ctx.throw(400, `Query param '${query_param_name}' must have at least ${min} items but has ${values.length}`)
    }
    if(max && values.length > max) {
        ctx.throw(400, `Query param '${query_param_name}' must have at most ${max} items but has ${values.length}`)
    }

    return values;
}

export function queryInteger(ctx, query_parameter_name, {min, max, default_value} = {}) {
    if(default_value !== undefined && typeof default_value !== 'number') {
        throw new Error('Default value must be an integer if specified');
    }
    if(min !== undefined && typeof min !== 'number') {
        throw new Error('Default value must be an integer if specified');
    }
    if(max !== undefined && typeof max !== 'number') {
        throw new Error('Default value must be an integer if specified');
    }

    const string = ctx.query[query_parameter_name];

    let integer;

    if(string === undefined) {
        if(!default_value) {
            ctx.throw(400, `Query parameter '${query_parameter_name}' is required`);
        }
        integer = default_value;
    } else {
        if(!string.match(/^[1-9]\d*$/)) {
            ctx.throw(400, `Query parameter '${query_parameter_name}' must be an integer but was '${string}'`);
        }
        integer = Number(string);

        if(min !== undefined && integer < min) {
            ctx.throw(400, `Query parameter '${query_parameter_name}' must be at least '${min}' but was '${integer}'`);
        }

        if(max !== undefined && integer > max) {
            ctx.throw(400, `Query parameter '${query_parameter_name}' must be at most '${max}' but was '${integer}'`);
        }
    }

    return integer;
}

export function paramMatch(ctx, query_parameter_name, pattern) {
    const value = ctx.params[query_parameter_name];
    const match = pattern.exec(value)
    if(!match) {
        ctx.throw(400, `Invalid param '${query_parameter_name}'`);
    }
    return value
}

export function accept(type) {
    return async (ctx, next) => {
        if (!ctx.is(type)) {
            ctx.throw(415, `Request most offer '${type}'`)
        }

        await bodyParser({ enableTypes: ['json'] })(ctx, next);
    }
}

export function offer(type) {
    return async (ctx, next) => {
        if (!ctx.accepts(type)) {
            ctx.throw(406, `Request must accept '${type}'`)
        }
        await next();
    }
}