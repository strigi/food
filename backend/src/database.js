import mongoose from "mongoose";

export async function connect() {
    const uri = process.env.MONGODB_URL;
    console.info(`Connecting to MongoDB '${uri}'`)
    await mongoose.connect(uri);
    mongoose.set('debug', true);
    console.info(`Connected to MongoDB '${uri}'`)
}

export async function disconnect() {
    console.info(`Disconnecting from MongoDB`)
    await mongoose.disconnect();
    console.info(`Disconnected from MongoDB`)
}