db = db.getSiblingDB('food')

// 2022-07-30
db.consumptions.insertOne({
    time: new ISODate('2022-07-30T02:00'),
    nutrient: db.nutrients.findOne({ name: 'Lactose Vrije Halfvolle Melk'})._id,
    quantity: 275
});

db.consumptions.insertOne({
    time: new ISODate('2022-07-30T09:50'),
    nutrient: db.nutrients.findOne({ name: 'Verse kaas (mager)'})._id,
    quantity: 294
});

db.consumptions.insertOne({
    time: new ISODate('2022-07-30T09:50'),
    nutrient: db.nutrients.findOne({ name: 'Cassonade Graeffe'})._id,
    quantity: 51
});

db.consumptions.insertOne({
    time: new ISODate('2022-07-30T10:26'),
    nutrient: db.nutrients.findOne({ name: 'Lactose Vrije Halfvolle Melk'})._id,
    quantity: 150
})

db.consumptions.insertOne({
    time: new ISODate('2022-07-30T10:26'),
    nutrient: db.nutrients.findOne({ name: 'Suiker (klontjes)'})._id,
    quantity: 2*6
})

// 13u00 Gemarineerde kip 120g (gebakken) sous 20g
// 13u00 Kroketten (gebakken oven) 250g
// 13u00 mayonaise: 30g
// 13u00 tomaat 100g
// 13u00 siroop drankje standaard portie mama
// 14u00 ijsje
// 15u00 aardbei 100g

db.consumptions.insertOne({
    time: new ISODate('2022-07-30T17:00'),
    nutrient: db.nutrients.findOne({ name: 'Coca-Cola (original)'})._id,
    quantity: 375
});

db.consumptions.insertOne({
    time: new ISODate('2022-07-30T18:25'),
    nutrient: db.nutrients.findOne({ name: 'Choco Toffees'})._id,
    quantity: 2*8
});

db.consumptions.insertOne({
    time: new ISODate('2022-07-30T19:59'),
    nutrient: db.nutrients.findOne({ name: 'Babybel Mini (original)'})._id,
    quantity: 2*22
});