db = db.getSiblingDB('food')

db.nutrients.insertOne({
    name: 'Lactose Vrije Halfvolle Melk',
    brand: 'Boni Selection',
    propertiesPer100g: {
        energy: 199, // kJ
        fat: 1.6, // of which sat. 1.0
        carbohydrate: 4.8, // of which sugar: 4.8
        fiber: 0,
        protein: 3.4,
        sodium: 0.11, // Na
        calcium: 0.12
    }
});

db.nutrients.insertOne({
    name: 'Tarwe Toasts (volkoren)',
    brand: 'Boni Selection',
    propertiesPer100g: {
        energy: 1587, // kJ
        fat: 4.3, // of which sat. 0.5
        carbohydrate: 68, // of which sugar: 2.8
        fiber: 8.5,
        protein: 12,
        sodium: 1.0, // Na
    }
});

db.nutrients.insertOne({
    name: 'Choco Toffees',
    brand: undefined,
    propertiesPer100g: {
        energy: 1949, // kJ
        fat: 21, // of which sat. 12
        carbohydrate: 66, // of which sugar: 45
        fiber: 2.5,
        protein: 3.6,
        sodium: 0.13, // Na
    }
});

db.nutrients.insertOne({
    name: 'Volle yoghurt',
    brand: 'Everyday',
    propertiesPer100g: {
        energy: 245, // kJ
        fat: 3.0, // of which sat. 1.8
        carbohydrate: 4.6, // of which sugar: 4.6
        fiber: 0,
        protein: 3.3,
        sodium: 0.13, // Na
        calcium: 0.12
    }
});

db.nutrients.insertOne({
    name: 'Gouda Jong (sneetjes)',
    brand: 'Everyday',
    propertiesPer100g: {
        energy: 1436, // kJ
        fat: 28, // of which sat. 19
        carbohydrate: 0, // of which sugar: 0
        fiber: 0,
        protein: 24,
        sodium: 1.8, // Na
    }
});

db.nutrients.insertOne({
    name: 'Gerookte Atlantische zalm (uit noorwegen)',
    brand: 'Everyday',
    propertiesPer100g: {
        energy: 813, // kJ
        fat: 13, // of which sat. 2.3
        carbohydrate: 0.6, // of which sugar: <0.5
        fiber: 0,
        protein: 20,
        sodium: 3, // Na
    }
});

db.nutrients.insertOne({
    name: 'Coca-Cola (original)',
    brand: 'Coca-Cola',
    propertiesPer100g: {
        energy: 180, // kJ
        fat: 0, // of which sat. 0
        carbohydrate: 10.6, // of which sugar: 10.6
        protein: 0,
        sodium: 0, // Na
    }
});

// Bruine suiker
db.nutrients.insertOne({
    name: 'Cassonade Graeffe',
    brand: 'Tienen',
    propertiesPer100g: {
        energy: 1637, // kJ
        fat: 0, // of which sat. 0
        carbohydrate: 94.4, // of which sugar: 94
        protein: 1.9,
        sodium: 0.16, // Na
    }
});

// Plattekaas
db.nutrients.insertOne({
    name: 'Verse kaas (mager)',
    brand: 'Everyday',
    propertiesPer100g: {
        energy: 199, // kJ
        fat: 0.5, // of which sat. 0
        carbohydrate: 3.5, // of which sugar: 3.5
        fiber: 0,
        protein: 8,
        sodium: 0.1, // Na
    }
});

db.nutrients.insertOne({
    name: 'Suiker (klontjes)',
    brand: 'Everyday',
    propertiesPer100g: {
        energy: 1700, // kJ
        fat: 0, // of which sat. 0
        carbohydrate: 100, // of which sugar: 100
        fiber: 0,
        protein: 0,
        sodium: 0, // Na
    }
});

db.nutrients.insertOne({
    name: 'Babybel Mini (original)',
    brand: 'Bel',
    propertiesPer100g: {
        energy: 1225, // kJ
        fat: 23, // of which sat. 15.5
        carbohydrate: 0.1, // of which sugar: 0.1
        protein: 22,
        sodium: 1.7, // Na
        calcium: 0.68, // Na
    }
});

db.nutrients.insertOne({
    name: 'Verse kaas met fruit',
    brand: 'Everyday',
    propertiesPer100g: {
        energy: 465, // kJ
        fat: 2.9, // of which sat. 1.8
        carbohydrate: 14, // of which sugar: 13
        protein: 7.3,
        fiber: 0.5,
        sodium: 0.09, // Na
        calcium: 240e-3, // Na
        vit_d: 1.2e-6
    }
});

// Blik 415g verschillende barcodes
db.nutrients.insertOne({
    name: 'Spaghetti bolognese',
    brand: 'Everyday',
    propertiesPer100g: {
        energy: 356, // kJ
        fat: 2.2, // of which sat. 0.3
        carbohydrate: 12, // of which sugar: 3
        protein: 3.8,
        fiber: 0.8,
        sodium: 1.1, // Na
    }
});
