export class User {
    constructor(spec) {
        Object.assign(this, spec)
    }

    get fullName() {
        return `${this.firstName} ${this.lastName}`
    }
}
